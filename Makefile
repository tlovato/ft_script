# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/09/19 01:41:43 by tlovato           #+#    #+#              #
#    Updated: 2018/06/15 16:42:06 by tlovato          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = ft_script

SRCS_DIR = srcs/
OBJS_DIR = objs/

SRCS_FILES = ft_script.c recup_params.c launch_typescript.c fd_communication.c\
	recup_paths.c playback.c
SRCS = $(addprefix $(SRCS_DIR), $(SRCS_FILES))
OBJS = $(SRCS:.c=.o)

all: $(NAME)

%.o: %.c
	@gcc -g3 -o $@ -c $<

$(NAME): $(OBJS)
		@echo "\033[1;36m --> Objects Done ! <--\033[m"
		@make -C libft/
		@echo "\033[1;32m --> libft Done ! <--\033[m"
		@gcc -o $@ $^ -L libft/ -lft 
		@echo "\033[1;35m --> ft_script Done ! <--\033[m"

clean:
		@rm -f $(OBJS)
		@echo "\033[1;33m --> Cleaning Done ! <--\033[m"

fclean: clean
		@rm -f $(NAME)
		@cd libft && make fclean
		@echo "\033[1;31m --> Fcleaning Done ! <--\033[m"

re: fclean all

.PHONY: all clean fclean re
