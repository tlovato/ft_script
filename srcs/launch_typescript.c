/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   launch_typescript.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/19 21:21:00 by tlovato           #+#    #+#             */
/*   Updated: 2018/06/15 17:33:07 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/ft_script.h"

static void				open_pty(int *fd_pty, int *fd_tty)
{
	char				name[256];

	*fd_pty = open("/dev/ptmx", O_RDWR | O_NOCTTY);
	ioctl(*fd_pty, TIOCPTYGRANT);
	ioctl(*fd_pty, TIOCPTYGNAME, name);
	ioctl(*fd_pty, TIOCPTYUNLK);
	*fd_tty = open(name, O_RDWR | O_NOCTTY);
}

static void				fork_slave(int fd_tty)
{
	setsid();
	ioctl(fd_tty, TIOCSCTTY, NULL);
	dup2(fd_tty, 0);
	dup2(fd_tty, 1);
	dup2(fd_tty, 2);
}

static void				display_message(char *act, char *file, int fd_file)
{
	struct stat			stat;

	if (g_opts && ft_strchr(g_opts, 'q'))
		return ;
	ft_putstr("Script ");
	ft_putstr(act);
	ft_putstr(", output file is ");
	ft_putendl(file);
	if (!g_opts || !ft_strchr(g_opts, 'r'))
	{
		ft_putstr_fd("Script ", fd_file);
		ft_putstr_fd(act, fd_file);
		ft_putstr_fd(" on ", fd_file);
		fstat(fd_file, &stat);
		ft_putstr_fd(ctime(&stat.st_mtime), fd_file);
	}
}

void					launch_typescript(int fd_file, t_params params)
{
	int					fd_pty;
	int					fd_tty;
	fd_set				fd_read;

	open_pty(&fd_pty, &fd_tty);
	g_pid = fork();
	if (!g_pid)
	{
		fork_slave(fd_tty);
		execve(params.cmd[0], params.cmd, params.env);
		ft_putstr_fd("ft_script: ", 2);
		ft_putstr_fd(params.cmd[0], 2);
		ft_putstr_fd(": No such file or directory\n", 2);
		exit(EXIT_FAILURE);
	}
	close(fd_tty);
	display_message("started", params.file, fd_file);
	FD_ZERO(&fd_read);
	while (waitpid(g_pid, NULL, WNOHANG) != g_pid)
		fd_communication(fd_pty, fd_read, fd_file);
	display_message("done", params.file, fd_file);
	close(fd_pty);
}
