/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fd_communication.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/01 23:51:12 by tlovato           #+#    #+#             */
/*   Updated: 2018/06/15 16:04:59 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/ft_script.h"

void			fd_communication(int fd_pty, fd_set fd_read, int fd_file)
{
	int			ret;
	char		buf[2048];

	FD_SET(0, &fd_read);
	FD_SET(fd_pty, &fd_read);
	select(fd_pty + 1, &fd_read, NULL, NULL, NULL);
	ret = 0;
	if (FD_ISSET(0, &fd_read))
		if ((ret = read(0, buf, 2048)))
		{
			if (g_opts && ft_strchr(g_opts, 'r'))
				record(fd_file, ret, 'i');
			write(fd_pty, buf, ret);
		}
	if (FD_ISSET(fd_pty, &fd_read))
		if ((ret = read(fd_pty, buf, 2048)))
		{
			write(1, buf, ret);
			if (g_opts && ft_strchr(g_opts, 'r'))
				record(fd_file, ret, 'o');
			write(fd_file, buf, ret);
		}
}
