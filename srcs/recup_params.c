/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   recup_params.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/19 03:21:08 by tlovato           #+#    #+#             */
/*   Updated: 2018/06/15 17:36:23 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/ft_script.h"

static int			verif_options(char *options)
{
	int				i;

	i = 0;
	while (options[i])
	{
		if (!ft_strchr("adpqr", options[i]))
		{
			ft_putstr_fd("ft_script: illegal option -- ", 2);
			ft_putchar_fd(options[i], 2);
			ft_putchar_fd('\n', 2);
			ft_putstr_fd("usage:", 2);
			ft_putstr_fd(" ./ft_script [-adpqr] [file [command...]]\n", 2);
			exit(EXIT_FAILURE);
		}
		i++;
	}
	return (1);
}

void				recup_params(int n, char **av, t_params *params)
{
	params->file = ft_strdup("typescript");
	params->cmd = recup_path_shell(params->env);
	if (n >= 1)
	{
		if (av[1][0] == '-' && verif_options(&av[1][1]))
			g_opts = ft_strdup(&av[1][1]);
		else
			params->file = ft_strdup(av[1]);
	}
	if (n >= 2)
	{
		if (g_opts)
			params->file = ft_strdup(av[2]);
		else
			params->cmd = recup_path_bin(&av[2], params);
	}
	if (n >= 3)
		params->cmd = recup_path_bin(&av[3], params);
}
