/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_script.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/19 01:41:51 by tlovato           #+#    #+#             */
/*   Updated: 2018/06/15 17:08:55 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/ft_script.h"

static t_params			init_params(char **env)
{
	t_params			new;

	g_opts = NULL;
	new.file = NULL;
	new.cmd = NULL;
	new.env = ft_cpy_tab(env);
	return (new);
}

static void				free_params(t_params *params, int fd_file)
{
	ft_free(g_opts);
	ft_free(params->file);
	ft_free_str_tab(params->cmd);
	ft_free_str_tab(params->env);
	close(fd_file);
}

static void				mode_raw(int activate)
{
	struct termios		term;

	ioctl(0, TIOCGETA, &term);
	if (activate)
		term.c_lflag &= ~(ECHO | ICANON);
	else
		term.c_lflag |= (ICANON | ECHO);
	ioctl(0, TIOCSETA, &term);
}

static int				create_fd(char *file)
{
	int					newfd;
	int					flags;

	if (g_opts && ft_strchr(g_opts, 'p'))
		flags = O_RDONLY;
	else if (g_opts && ft_strchr(g_opts, 'a'))
		flags = O_RDWR | O_APPEND;
	else
		flags = O_RDWR | O_TRUNC;
	if ((newfd = open(file, flags)) == -1)
	{
		if (g_opts && ft_strchr(g_opts, 'p'))
		{
			ft_putstr_fd("ft_script: ", 2);
			ft_putstr_fd(file, 2);
			ft_putstr_fd(": No such file or directory\n", 2);
			exit(EXIT_FAILURE);
		}
		newfd = open(file, O_CREAT | O_RDWR);
	}
	flags = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
	fchmod(newfd, (g_opts && ft_strchr(g_opts, 'p')) ? flags | S_IWUSR : flags);
	return (newfd);
}

int						main(int ac, char **av, char **env)
{
	int					fd_file;
	t_params			params;
	struct termios		save;
	struct termios		term;
	struct stat			stat;

	params = init_params(env);
	recup_params(ac - 1, av, &params);
	fd_file = create_fd(params.file);
	mode_raw(1);
	if (g_opts && ft_strchr(g_opts, 'p'))
	{
		fstat(fd_file, &stat);
		playback(fd_file, stat.st_size);
	}
	else
		launch_typescript(fd_file, params);
	mode_raw(0);
	free_params(&params, fd_file);
	return (0);
}
