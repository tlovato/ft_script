/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   playback.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/24 20:56:13 by tlovato           #+#    #+#             */
/*   Updated: 2018/06/15 17:09:20 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/ft_script.h"

void					record(int fd, size_t len, char io)
{
	t_record			datas;
	struct timeval		timeval;

	gettimeofday(&timeval, NULL);
	datas.len = len;
	datas.rec_time.tv_sec = timeval.tv_sec;
	datas.rec_time.tv_nsec = timeval.tv_usec * 1000;
	datas.io = io;
	write(fd, &datas, sizeof(t_record));
}

static void				invalid_stamp(void)
{
	ft_putstr_fd("ft_script: invalid stamp.\n", 2);
	exit(EXIT_FAILURE);
}

static void				sleep_input(t_record rec, struct timespec prec)
{
	struct timespec		sleep;

	sleep.tv_sec = rec.rec_time.tv_sec - prec.tv_sec;
	sleep.tv_nsec = rec.rec_time.tv_nsec - prec.tv_nsec;
	if (sleep.tv_nsec < 0)
	{
		sleep.tv_sec -= 1;
		sleep.tv_nsec += 1000000000;
	}
	nanosleep(&sleep, NULL);
}

void					playback(int fd_file, off_t size)
{
	int					i;
	int					ret;
	t_record			rec;
	struct timespec		prec;
	char				buf[2048];

	i = 0;
	while (i < size)
	{
		ret = read(fd_file, &rec, sizeof(t_record));
		if (rec.io != 'o' && rec.io != 'i')
			invalid_stamp();
		if (rec.io == 'o')
		{
			read(fd_file, &buf, rec.len);
			write(STDOUT_FILENO, buf, rec.len);
		}
		if (rec.io == 'i' && !ft_strchr(g_opts, 'd'))
			sleep_input(rec, prec);
		prec.tv_sec = rec.rec_time.tv_sec;
		prec.tv_nsec = rec.rec_time.tv_nsec;
		i += ret + rec.len;
	}
}
