/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   recup_paths.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/12 19:45:04 by tlovato           #+#    #+#             */
/*   Updated: 2018/06/15 17:56:30 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/ft_script.h"

static char			*find_var_in_env(char *find, int n, char **env)
{
	int				i;

	i = -1;
	while (env[++i])
		if (!ft_strncmp(find, env[i], n))
			return (ft_strdup(&env[i][n + 1]));
	return (NULL);
}

char				**recup_path_shell(char **env)
{
	char			*shell[3];

	shell[0] = find_var_in_env("SHELL", 5, env);
	shell[1] = ft_strdup("-i");
	shell[2] = NULL;
	return (ft_cpy_tab(shell));
}

static char			*find_path(char **path_tab, char *cmd)
{
	int				i;
	char			*path_bin;

	i = 0;
	path_bin = NULL;
	while (path_tab[i])
	{
		path_bin = ft_strjoin(path_tab[i], "/");
		path_bin = ft_strjoinfree(path_bin, cmd, 1);
		if (!access(path_bin, X_OK))
			break ;
		ft_free(path_bin);
		path_bin = NULL;
		i++;
	}
	ft_free_str_tab(path_tab);
	return ((path_bin) ? path_bin : cmd);
}

char				**recup_path_bin(char **av, t_params *params)
{
	char			*path;
	char			**path_tab;
	char			**ret;
	int				i;

	if (params->cmd)
		ft_free_str_tab(params->cmd);
	path = find_var_in_env("PATH", 4, params->env);
	path_tab = ft_strsplit(path, ':');
	ret = (char **)ft_malloc(ft_strtablen(av) * (sizeof(char *)));
	ret[0] = find_path(path_tab, av[0]);
	i = 0;
	while (av[++i])
		ret[i] = ft_strdup(av[i]);
	ret[i] = NULL;
	ft_free(path);
	return (ret);
}
