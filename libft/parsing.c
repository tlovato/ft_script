/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing_case.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/23 12:05:19 by tlovato           #+#    #+#             */
/*   Updated: 2016/05/23 12:05:23 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

void			convtype_case(t_result *ret, va_list ap)
{
	char		*tmp;

	pars_arg(ap, ret);
	tmp = ft_strnew(ret->res_len + ret->conv_len);
	ft_memcpy(tmp, ret->res, ret->res_len);
	ft_memcpy(&tmp[ret->res_len], ret->conv, ret->conv_len);
	ret->res_len = ret->res_len + ret->conv_len;
	ret->res = tmp;
}

static void		attribarg(t_result *ret, va_list ap)
{
	if (is_flag(ret->arg[ret->i]))
	{
		ret->argument = 1;
		attrib_flags(ret->arg, &ret->argum);
	}
	else if (is_convtype(ret->arg[ret->i]))
		attrib_convtype(ret, ap);
}

void			invalid_arg(t_result *ret, int i)
{
	char		*tmp;

	inv_arg_case(ret);
	tmp = ft_strnew(ret->res_len + ret->conv_len);
	ft_memcpy(tmp, ret->res, ret->res_len);
	ft_memcpy(&tmp[ret->res_len], ret->conv, ret->conv_len);
	ret->res_len = ret->res_len + ret->conv_len;
	ret->res = tmp;
}

static void		tmpjoin(t_result *ret)
{
	char		*tmp;

	tmp = ft_strnew(ret->conv_len + ret->sub_len);
	ft_memcpy(tmp, ret->conv, ret->conv_len);
	ft_memcpy(&tmp[ret->conv_len], ret->sub, ret->sub_len);
	ret->conv = tmp;
	ret->conv_len = ret->conv_len + ret->sub_len;
}

void			pars_arg(va_list ap, t_result *r)
{
	int			j;

	r->i = 0;
	attrib_values_set(&r->set);
	set_types_values(&r->set, r->arg);
	attrib_val_flags(&r->argum);
	if (ft_strlen(r->arg) > 0)
		while (r->arg[r->i])
		{
			if (is_modif(r->arg[r->i]) && r->set.modif == NULL)
			{
				j = (r->arg[r->i + 1] == 'h' || r->arg[r->i + 1] == 'l');
				r->set.modif = ft_strsub(r->arg, r->i, 1 + j);
			}
			attribarg(r, ap);
			r->i++;
		}
	else
		return ;
	if (r->argument == 1)
		print_flags(r, r->argum, r->set);
	if (r->set.type != 'c' && r->set.type != 's')
		r->conv_len = ft_strlen(r->conv);
	attrib_val_flags(&r->argum);
	tmpjoin(r);
}
