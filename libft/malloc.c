/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/17 23:26:40 by tlovato           #+#    #+#             */
/*   Updated: 2018/06/15 16:42:58 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_malloc.h"

static t_header		*find_free(t_header **last, size_t size, t_header *base_lst)
{
	t_header		*tmp;

	tmp = base_lst;
	while (tmp && !(tmp->free && tmp->size >= size))
	{
		*last = tmp;
		tmp = tmp->next;
	}
	return (tmp);
}

static void			*alloc(t_header **base_lst, size_t size, size_t zone)
{
	t_header		*header;
	t_header		*last;
	void			*addr;
	size_t			len;

	if (!(*base_lst))
	{
		len = (zone) ? zone : size + sizeof(t_header *);
		if (!((*base_lst) = init(NULL, len)))
			return (NULL);
		return (new_block((void *)*base_lst, size, NULL, zone) + 1);
	}
	last = *base_lst;
	if (!(header = find_free(&last, size, *base_lst)))
	{
		addr = (void *)(last + 1) + last->size;
		if (zone)
			header = new_block(addr, size, last, zone);
		else
			header = new_large(size, last);
		last->next = header;
	}
	return (header + 1);
}

void				*ft_malloc(size_t size)
{
	if (size >= 1)
	{
		if (size <= S_TINY)
		{
			return (alloc(&g_base.tiny, size, Z_TINY));
		}
		else if (size <= S_SMALL)
			return (alloc(&g_base.small, size, Z_SMALL));
		else
			return (alloc(&g_base.large, size, 0));
	}
	return (NULL);
}
