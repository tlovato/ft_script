/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils2.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/23 13:41:02 by tlovato           #+#    #+#             */
/*   Updated: 2016/05/23 13:41:04 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

int			get_digits(char *arg, int i)
{
	int		digits;
	int		j;
	int		k;
	char	*cpydigits;

	digits = 0;
	j = 0;
	k = i;
	while (ft_isdigit(arg[i]))
		i++;
	cpydigits = ft_strnew(i);
	while (ft_isdigit(arg[k]))
	{
		cpydigits[j] = arg[k];
		j++;
		k++;
	}
	cpydigits[j] = '\0';
	digits = ft_atoi(cpydigits);
	ft_free(cpydigits);
	return (digits);
}

int			calcul_perc(t_result *ret)
{
	int		i;

	i = 0;
	while (ret->arg[i])
	{
		if (ret->arg[i] == '%')
			ret->perc++;
		i++;
	}
	return (i);
}

int			verif_invalid_arg(char *arg)
{
	int		i;

	i = 0;
	if (arg == NULL)
		return (0);
	while (arg[i])
	{
		if (arg[i] == '%' || ft_isalpha(arg[i]))
			return (1);
		else if (is_flag(arg[i]))
		{
			while (is_flag(arg[i]))
				i++;
			if (arg[i] == '%' || ft_isalpha(arg[i]))
				return (1);
			else
				return (0);
		}
		else
			return (0);
	}
	return (0);
}

int			verif_convtype(char *arg)
{
	int		i;

	i = 0;
	if (arg == NULL)
		return (0);
	while (arg[i])
	{
		if (is_convtype(arg[i]))
			return (1);
		i++;
	}
	return (0);
}

int			is_arg(int i)
{
	if (is_flag(i) || is_modif(i) || is_convtype(i) || is_precision(i)
			|| is_width(i))
		return (1);
	else
		return (0);
}
