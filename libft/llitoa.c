/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils6.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/25 23:17:21 by tlovato           #+#    #+#             */
/*   Updated: 2016/05/25 23:17:23 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

static long	long	ft_intlen(long long n)
{
	long long		len;

	len = n <= 0;
	while (n != 0)
	{
		n = n / 10;
		len++;
	}
	return (len);
}

char				*ft_llitoa(long long n)
{
	char			*str;
	long long		len;

	if ((str = (char *)ft_malloc(ft_intlen(n) + 1 * (sizeof(char)))) == NULL)
		return (NULL);
	str[0] = n < 0 ? '-' : '0';
	len = ft_intlen(n);
	n = n > 0 ? -n : n;
	str[len] = '\0';
	while (len - (str[0] == '-'))
	{
		str[--len] = -(n % 10) + 48;
		n = n / 10;
	}
	return (str);
}
