/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   flags.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/23 13:30:08 by tlovato           #+#    #+#             */
/*   Updated: 2016/05/23 13:30:09 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

static void		pars_flags(char *arg, t_flag *argum)
{
	if (arg[argum->i] == '#')
		argum->sharp = 1;
	if (arg[argum->i] == '0')
	{
		argum->zero = 1;
		if (argum->dash == 1)
			argum->zero = 0;
	}
	if (arg[argum->i] == '-')
		argum->dash = 1;
	if (arg[argum->i] == '+')
	{
		argum->plus = 1;
		if (argum->space == 1)
			argum->space = 0;
	}
	if (arg[argum->i] == ' ')
	{
		argum->space = 1;
		if (argum->plus == 1)
			argum->space = 0;
	}
}

static void		take_width(char *arg, t_flag *argum)
{
	int		i;

	i = 0;
	while (arg[i])
	{
		if (arg[i] >= '1' && arg[i] <= '9' && argum->width == 0)
		{
			if (arg[i - 1] == '.')
				break ;
			argum->width = get_digits(arg, i);
			while (ft_isdigit(arg[i]))
				i++;
		}
		i++;
	}
}

static void		take_prec(char *arg, t_flag *argum)
{
	int			i;

	i = 0;
	while (arg[i])
	{
		if (arg[i] == '.')
		{
			i++;
			if (ft_isdigit(arg[i]))
				argum->prec = get_digits(arg, i);
			else
				argum->prec = 0;
			while (ft_isdigit(arg[i]))
				i++;
		}
		i++;
	}
}

void			attrib_flags(char *arg, t_flag *a)
{
	a->i = 0;
	attrib_val_flags(a);
	while (arg[a->i])
	{
		if (arg[a->i] == '#' || ((arg[a->i] == '0' && arg[a->i - 1]
			!= '.' && (ft_isdigit(arg[a->i - 1]) == 0)) || arg[a->i] == '-'
			|| arg[a->i] == '+' || arg[a->i] == ' '))
			pars_flags(arg, a);
		if ((arg[a->i] >= '1' && arg[a->i] <= '9')
				&& (arg[a->i - 1] != '.') && a->is_width == 0)
		{
			a->is_width = 1;
			take_width(arg, a);
			while (ft_isdigit(arg[a->i]))
				a->i++;
		}
		if (arg[a->i] == '.')
		{
			a->is_prec = 1;
			take_prec(arg, a);
			while (ft_isdigit(arg[a->i]))
				a->i++;
		}
		a->i++;
	}
}
