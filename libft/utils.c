/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/23 12:08:22 by tlovato           #+#    #+#             */
/*   Updated: 2016/05/23 12:08:23 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

void	attrib_values_ret(t_result *ret)
{
	ret->cpy = NULL;
	ret->arg = NULL;
	ret->res = NULL;
	ret->res_len = 0;
	ret->sub = NULL;
	ret->sub_len = 0;
	ret->perc = 0;
	ret->len = 0;
	ret->conv = NULL;
	ret->conv_len = 0;
	ret->argument = 0;
	ret->percent = 0;
}

void	attrib_values_set(t_set *values)
{
	values->len = 0;
	values->type = 0;
	values->modif = NULL;
	values->percent = 0;
	values->index = 0;
}

void	attrib_val_flags(t_flag *argum)
{
	argum->sharp = 0;
	argum->zero = 0;
	argum->dash = 0;
	argum->plus = 0;
	argum->space = 0;
	argum->is_width = 0;
	argum->width = 0;
	argum->is_prec = 0;
	argum->prec = 0;
}

void	free_result(t_result *ret)
{
	if (ret)
	{
		if (ret->cpy)
			ft_free(ret->cpy);
		if (ret->arg)
			ft_free(ret->arg);
		if (ret->res)
			ft_free(ret->res);
		if (ret->sub)
			ft_free(ret->sub);
		ft_free(ret);
	}
}

void	set_types_values(t_set *values, char *arg)
{
	int		i;

	i = 0;
	while (arg[i])
	{
		if (is_convtype(arg[i]))
			values->type = arg[i];
		i++;
	}
}
