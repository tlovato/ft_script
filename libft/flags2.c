/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   flags2.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/01 18:18:48 by tlovato           #+#    #+#             */
/*   Updated: 2016/06/01 18:18:49 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

void			prec_udigits(t_flag a, t_result *ret, t_set val)
{
	int			i;
	int			len;
	char		*tmp;

	i = (val.type == 'x' || val.type == 'X') && a.sharp && ret->conv[2] != '\0';
	if (ret->conv[0] == '0' && a.prec == 0 && !i)
	{
		ret->conv[0] = '\0';
		return ;
	}
	len = a.prec - ft_strlen(ret->conv) + (2 * i);
	if (len <= 0)
		return ;
	tmp = ft_strnew(a.prec);
	if (i)
		tmp = ft_memcpy(tmp, ret->conv, 2);
	ft_memset(i ? &tmp[2] : tmp, '0', len);
	ft_strcat(tmp, i ? &ret->conv[2] : ret->conv);
	ft_free(ret->conv);
	ret->conv = tmp;
}

void			width_udigits(t_flag argum, t_result *ret)
{
	int			i;
	int			j;
	int			len;
	char		*tmp;
	int			k;

	i = (ret->conv[1] == 'x' || ret->conv[1] == 'X');
	k = argum.dash == 0 && argum.zero == 1 && argum.is_prec == 0;
	i = 2 * (i && k);
	len = argum.width - ft_strlen(ret->conv);
	if (len <= 0)
		return ;
	j = argum.dash == 1 ? ft_strlen(ret->conv) : 0;
	tmp = ft_strnew(argum.width);
	if (i)
		ft_memcpy(tmp, ret->conv, i);
	ft_memset(&tmp[i ? i : j], argum.zero == 1 ? '0' : ' ', len);
	if (j && !i)
		ft_memcpy(tmp, ret->conv, ft_strlen(ret->conv));
	else
		ft_strcat(tmp, &ret->conv[i]);
	ft_free(ret->conv);
	ret->conv = tmp;
}

void			flags_str(t_result *ret, t_flag a, t_set values)
{
	char		*tmp;
	int			len;

	if (a.is_prec == 1 && a.prec < ft_strlen(ret->conv) && values.type != 'c')
	{
		ret->conv[a.prec] = '\0';
		ret->conv_len = a.prec;
	}
	len = ret->conv_len;
	if (a.is_width == 1 && a.width > len)
	{
		tmp = ft_strnew(a.width);
		ft_memset(&tmp[a.dash == 1 ? len : 0], ' ', a.width - len);
		ft_memcpy(&tmp[a.dash == 1 ? 0 : a.width - len], ret->conv, len);
		ret->conv_len = a.width;
		ft_free(ret->conv);
		ret->conv = tmp;
	}
}

static void		prec_p(int prec, t_result *ret)
{
	int			len;
	char		*tmp;

	if (ret->conv[2] == '0' && prec == 0)
	{
		ret->conv[2] = '\0';
		return ;
	}
	len = prec - (ft_strlen(ret->conv) - 2);
	if (len <= 0)
		return ;
	tmp = ft_strnew(len + ft_strlen(ret->conv));
	tmp[0] = '0';
	tmp[1] = 'x';
	ft_memset(&tmp[2], '0', len);
	ft_strcat(tmp, &ret->conv[2]);
	ft_free(ret->conv);
	ret->conv = tmp;
}

void			flags_p(t_result *ret, t_flag argum)
{
	if (argum.is_prec == 1)
		prec_p(argum.prec, ret);
	if (argum.is_width == 1)
		width_udigits(argum, ret);
}
