/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils5.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/25 23:07:05 by tlovato           #+#    #+#             */
/*   Updated: 2016/05/25 23:07:08 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

char			*bin_to_str(unsigned int n)
{
	int			i;
	int			j;
	char		*str;

	i = 0;
	j = count_len(n);
	str = (char *)ft_malloc(sizeof(char) * j + 1);
	while (i < j)
	{
		str[i] = (n & 1) + '0';
		n = n >> 1;
		i++;
	}
	str[i] = '\0';
	return (ft_strrev(str));
}
