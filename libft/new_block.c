/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   new_block.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/17 23:37:05 by tlovato           #+#    #+#             */
/*   Updated: 2018/06/15 16:43:49 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_malloc.h"

t_header			*init(void *addr, size_t size)
{
	t_header		*new;
	int				prot;
	int				flags;

	prot = PROT_READ | PROT_WRITE | PROT_EXEC;
	flags = MAP_PRIVATE | MAP_ANON;
	if ((new = mmap(addr, size, prot, flags, -1, 0)) == (void *)-1)
		return (NULL);
	return ((void *)new);
}

t_header			*new_block(void *a, size_t s, t_header *last, size_t zone)
{
	t_header		*new;
	void			*start_page;

	start_page = (last) ? last->addr_page : a;
	if (a <= (void *)start_page + zone)
	{
		new = a;
		new->addr_page = start_page;
	}
	else
	{
		if (!(new = init((void *)start_page + zone, zone)))
			return (NULL);
		new->addr_page = (void *)new;
	}
	new->size = s;
	new->free = 0;
	new->prev = last;
	new->next = NULL;
	return (new);
}

t_header			*new_large(size_t size, t_header *last)
{
	t_header		*new;

	if (!(new = init(NULL, size + sizeof(t_header *))))
		return (NULL);
	new->addr_page = (void *)new;
	new->size = size;
	new->free = 0;
	new->prev = last;
	new->next = NULL;
	return (new);
}
