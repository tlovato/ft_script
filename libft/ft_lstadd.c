/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstadd.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/13 13:01:00 by tlovato           #+#    #+#             */
/*   Updated: 2015/12/14 13:27:40 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstadd(t_list **alst, t_list *newlst)
{
	if (*alst != NULL && newlst != NULL)
	{
		newlst->next = *alst;
		*alst = newlst;
	}
	else
		*alst = newlst;
}
