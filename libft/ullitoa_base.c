/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ullitoa_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/25 23:21:04 by tlovato           #+#    #+#             */
/*   Updated: 2018/06/15 16:03:15 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

static unsigned long long		ft_intlen(unsigned long long n, int base)
{
	unsigned long long			len;

	len = 0;
	if (n == 0)
		return (1);
	while (n > 0)
	{
		n = n / base;
		len++;
	}
	return (len);
}

static char						*ft_core(unsigned long long n, char *s, int b)
{
	unsigned long long			len;
	char						*tab;

	tab = "0123456789ABCDEF";
	len = ft_intlen(n, b);
	s[len] = '\0';
	while (n != 0)
	{
		s[--len] = tab[n % b];
		n = n / b;
	}
	return (s);
}

char							*ft_ullitoa_base(unsigned long long nb, int b)
{
	char						*s;
	unsigned long long			i;
	unsigned long long			n;

	if ((s = (char *)ft_malloc(ft_intlen(nb, b) + 1 * (sizeof(char)))) == NULL)
		return (NULL);
	if (b < 2 || b > 16)
		return (NULL);
	i = 0;
	n = nb;
	if (n == 0)
	{
		s[0] = '0';
		s[1] = '\0';
		return (s);
	}
	return (ft_core(n, s, b));
}
