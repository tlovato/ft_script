/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   realloc.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/16 17:16:05 by tlovato           #+#    #+#             */
/*   Updated: 2018/06/15 16:43:38 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_malloc.h"

void			*realloc(void *ptr, size_t size)
{
	t_header	*ptr_header;
	void		*new;

	if (!ptr)
		return (ft_malloc(size));
	if ((ptr_header = verif_base(ptr, g_base.tiny))
		|| (ptr_header = verif_base(ptr, g_base.small))
		|| (ptr_header = verif_base(ptr, g_base.large)))
	{
		if (ptr_header->size >= size)
			return (ptr);
		if (!(new = ft_malloc(size)))
			return (ptr);
		ft_memcpy(new, ptr, ptr_header->size);
		ft_free(ptr);
		return (new);
	}
	return (NULL);
}
