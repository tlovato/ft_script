/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_fd.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/27 17:39:00 by tlovato           #+#    #+#             */
/*   Updated: 2017/10/25 21:41:34 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char			*read_fd(int fd, char stop)
{
	char		buf[2];
	char		*newstock;
	char		*cat;

	newstock = ft_strnew(1);
	ft_bzero(buf, 2);
	while ((stop) ? (read(fd, buf, 1) && buf[0] != stop) : read(fd, buf, 1))
	{
		cat = ft_strnew(1);
		buf[1] = '\0';
		newstock = ft_strjoinfree(newstock, cat, 3);
	}
	return (newstock);
}
