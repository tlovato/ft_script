/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_on_file.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/28 22:44:13 by tlovato           #+#    #+#             */
/*   Updated: 2017/09/29 00:31:59 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void			print_on_file(char *file, char *str)
{
	int			fd;

	if ((fd = open(file, O_RDWR | O_APPEND)) == -1)
		fd = open(file, O_CREAT | O_RDWR | O_APPEND);
	write(fd, str, ft_strlen(str));
}
