/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_addr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/05 18:17:28 by Pyxis             #+#    #+#             */
/*   Updated: 2018/06/15 16:43:58 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_malloc.h"

static int		ft_intlen(int n)
{
	int			len;

	len = 0;
	if (n == 0)
		return (1);
	while (n > 0)
	{
		n = n / 16;
		len++;
	}
	return (len);
}

static void		itoa_addr(int addr, char *buf)
{
	int			len;
	char		*tab;

	tab = "0123456789ABCDEF";
	len = ft_intlen(addr);
	buf[len] = '\0';
	while (addr != 0)
	{
		buf[--len] = tab[addr % 16];
		addr = addr / 16;
	}
}

void			print_addr(int addr)
{
	char		buf[20];

	itoa_addr(addr, buf);
	ft_putstr("0x");
	ft_putstr(buf);
}
