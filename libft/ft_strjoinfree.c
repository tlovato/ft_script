/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoinfree.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/10 16:32:25 by tlovato           #+#    #+#             */
/*   Updated: 2017/09/27 17:24:47 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strjoinfree(char *s1, char *s2, char i)
{
	char	*tmp;

	tmp = ft_strjoin(s1, s2);
	if (i == 1 || i == 3)
		ft_free(s1);
	if (i == 2 || i == 3)
		ft_free(s2);
	return (tmp);
}
