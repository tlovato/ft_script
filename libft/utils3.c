/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils3.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/25 10:25:37 by tlovato           #+#    #+#             */
/*   Updated: 2016/05/25 10:25:39 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

int			is_convtype(int i)
{
	if (i == 's' || i == 'S' || i == 'p' || i == 'd' || i == 'D' || i == 'i' ||
			i == 'o' || i == 'O' || i == 'u' || i == 'U' || i == 'x' ||
			i == 'X' || i == 'c' || i == 'C' || i == 'b')
		return (1);
	else
		return (0);
}

int			is_flag(int i)
{
	if (i == '-' || i == '+' || i == '0' || i == ' ' || i == '#' || i == '.'
			|| ft_isdigit(i))
		return (1);
	else
		return (0);
}

void		attrib_values_type(t_type *pars)
{
	pars->wide = NULL;
	pars->ret = 0;
	pars->p = NULL;
	pars->s = NULL;
	pars->smaj = NULL;
	pars->percent = 0;
	pars->si_conv = 0;
	pars->usi_conv = 0;
}

void		take_invalid_arg(t_result *ret, int i)
{
	int			j;

	j = i;
	ft_free(ret->arg);
	while (ret->cpy[j] && is_flag(ret->cpy[j]))
		j++;
	ret->arg = ft_strsub(ret->cpy, i, j - i);
}

int			is_precision(int i)
{
	if (i == '.')
		return (1);
	else
		return (0);
}
