/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/05 11:21:42 by tlovato           #+#    #+#             */
/*   Updated: 2018/06/15 17:57:25 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

static int		ft_countw(const char *s, char c)
{
	int		i;
	int		j;

	i = 0;
	j = 0;
	while (s[i])
	{
		while ((s[i] == c) && (s[i]))
			i++;
		if ((s[i] != c) && (s[i]))
		{
			i++;
			j++;
			while ((s[i] != c) && (s[i]))
				i++;
		}
	}
	return (j);
}

static int		ft_countwlen(const char *s, char c)
{
	int		i;

	i = 0;
	while ((s[i] != c) && (s[i]))
		i++;
	return (i);
}

char			**ft_strsplit(const char *s, char c)
{
	char	**tab;
	int		n;
	int		i;
	int		j;
	int		len;

	n = ft_countw(s, c);
	i = 0;
	j = 0;
	tab = (char **)ft_malloc(sizeof(char *) * (n + 1));
	if (tab == NULL)
		return (NULL);
	while (i < n)
	{
		while (s[j] == c)
			j++;
		len = ft_countwlen(&s[j], c);
		tab[i] = (char *)ft_malloc(sizeof(char *) * (len + 1));
		ft_memcpy(tab[i], &s[j], len);
		tab[i][len] = '\0';
		j = j + len;
		i++;
	}
	tab[i] = NULL;
	return (tab);
}
