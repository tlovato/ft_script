/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   convtype.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/25 15:47:35 by tlovato           #+#    #+#             */
/*   Updated: 2016/05/25 15:47:37 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

static void		break_maj(t_result *ret)
{
	if (ret->set.type >= 'A' && ret->set.type < 'X')
		ret->set.modif = ft_strdup("l");
	if (ret->set.type != 'X')
		ret->set.type = ft_tolower(ret->set.type);
}

static void		modif_parsing(t_result *ret, t_type *p, va_list ap, t_set val)
{
	if (ft_strequ(ret->set.modif, "hh"))
		hh_conv(p, ap, val);
	else if (ft_strequ(ret->set.modif, "h"))
		h_conv(p, ap, val);
	else if (ft_strequ(ret->set.modif, "ll"))
		ll_conv(p, ap, val);
	else if (ft_strequ(ret->set.modif, "l"))
		l_conv(p, ap, val, ret);
	else if (ft_strequ(ret->set.modif, "j") || ft_strequ(ret->set.modif, "z"))
		zj_conv(p, ap, val);
}

static void		attrib_conv(t_result *ret, t_type *pars, va_list ap)
{
	if (ret->set.type == 'c')
		pars->si_conv = (char)va_arg(ap, int);
	if (ret->set.type == 'd' || ret->set.type == 'i')
		pars->si_conv = va_arg(ap, int);
	if (ret->set.type == 'o' || ret->set.type == 'x' || ret->set.type == 'X'
		|| ret->set.type == 'u')
		pars->usi_conv = va_arg(ap, unsigned);
	if (ret->set.type == 'p')
		pars->p = va_arg(ap, void *);
	if (ret->set.type == 's')
		pars->s = va_arg(ap, char *);
	if (ret->set.type == 'b')
		pars->usi_conv = va_arg(ap, long long);
}

void			attrib_convtype(t_result *ret, va_list ap)
{
	t_type		*pars;

	pars = (t_type *)ft_malloc(sizeof(t_type));
	attrib_values_type(pars);
	break_maj(ret);
	if (ret->set.modif != NULL)
		modif_parsing(ret, pars, ap, ret->set);
	else
		attrib_conv(ret, pars, ap);
	pars_print_conv(pars, ret);
	ft_free(pars);
}
