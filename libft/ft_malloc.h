/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_malloc.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/22 14:20:31 by tlovato           #+#    #+#             */
/*   Updated: 2018/06/15 16:41:18 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_MALLOC_H
# define FT_MALLOC_H

# define S_TINY			(size_t)getpagesize() * 2
# define S_SMALL		(size_t)getpagesize() * 16
# define Z_TINY			(size_t)(S_TINY + sizeof(t_header)) * 100
# define Z_SMALL		(size_t)(S_SMALL + sizeof(t_header)) * 100

# define PROT			PROT_READ | PROT_WRITE | PROT_EXEC
# define FLAGS 			MAP_PRIVATE | MAP_ANON

# include <sys/mman.h>
# include "libft.h"
# include <stdio.h>
# include <errno.h>

typedef struct			s_header
{
	size_t				size;
	int					free;
	void				*addr_page;
	struct s_header		*prev;
	struct s_header		*next;
}						t_header;

typedef struct			s_base
{
	t_header			*tiny;
	t_header			*small;
	t_header			*large;
}						t_base;

struct s_base			g_base;

void					ft_free(void *ptr);
void					*ft_malloc(size_t size);
void					*realloc(void *ptr, size_t size);
void					show_alloc_mem();

t_header				*init(void *addr, size_t size);
t_header				*new_block(void *a, size_t s, t_header *l, size_t zone);
t_header				*new_large(size_t size, t_header *last);

void					print_addr(int addr);
t_header				*verif_base(void *ptr, t_header *base);

#endif
