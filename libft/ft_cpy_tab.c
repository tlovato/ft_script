/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cpy_tab.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/23 17:49:37 by tlovato           #+#    #+#             */
/*   Updated: 2017/10/11 19:00:35 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		**ft_cpy_tab(char **tab)
{
	char	**ret;
	int		i;

	i = 0;
	if (!(ret = (char **)ft_malloc(sizeof(char *) * ft_strtablen(tab) + 1)))
		return (NULL);
	while (i < ft_strtablen(tab))
	{
		ret[i] = ft_strdup(tab[i]);
		i++;
	}
	ret[i] = NULL;
	return (ret);
}
