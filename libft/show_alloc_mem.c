/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   show_alloc_mem.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/22 14:17:59 by tlovato           #+#    #+#             */
/*   Updated: 2018/06/15 16:43:27 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_malloc.h"

static int		display_infos(t_header *base)
{
	t_header	*tmp;
	int			total;

	total = 0;
	tmp = base;
	print_addr((int)tmp);
	ft_putendl("");
	while (tmp)
	{
		print_addr((int)((void *)(tmp + 1)));
		ft_putstr(" - ");
		print_addr((int)((void *)(tmp + 1) + tmp->size));
		ft_putstr(" : ");
		ft_putunbr(tmp->size);
		ft_putendl(" octets");
		total = total + tmp->size;
		tmp = tmp->next;
	}
	return (total);
}

void			show_alloc_mem(void)
{
	int			total;

	total = 0;
	if (g_base.tiny)
	{
		ft_putstr("TINY : ");
		total = total + display_infos(g_base.tiny);
		ft_putchar('\n');
	}
	if (g_base.small)
	{
		ft_putstr("SMALL : ");
		total = total + display_infos(g_base.small);
		ft_putchar('\n');
	}
	if (g_base.large)
	{
		ft_putstr("LARGE : ");
		total = total + display_infos(g_base.large);
		ft_putchar('\n');
	}
	ft_putstr("Total : ");
	ft_putunbr(total);
	ft_putendl(" octets");
}
