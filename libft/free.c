/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/17 23:26:18 by tlovato           #+#    #+#             */
/*   Updated: 2018/06/15 16:43:16 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_malloc.h"

t_header		*verif_base(void *ptr, t_header *base)
{
	t_header	*tmp;

	tmp = base;
	while (tmp)
	{
		if ((void *)tmp + sizeof(t_header) == ptr)
			return (tmp);
		tmp = tmp->next;
	}
	return (NULL);
}

void			ft_free(void *ptr)
{
	t_header	*ptr_header;

	if (!ptr)
		return ;
	if ((ptr_header = verif_base(ptr, g_base.large)))
		munmap(ptr_header, sizeof(t_header *) + ptr_header->size);
	else if ((ptr_header = verif_base(ptr, g_base.tiny))
			|| (ptr_header = verif_base(ptr, g_base.small)))
	{
		ptr_header->free = 1;
	}
}
