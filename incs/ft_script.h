/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_script.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/19 01:41:59 by tlovato           #+#    #+#             */
/*   Updated: 2018/06/15 16:42:20 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SCRIPT_H
# define FT_SCRIPT_H

# include <sys/xattr.h>
# include <sys/ioctl.h>
# include <sys/stat.h>
# include <sys/types.h>
# include <sys/select.h>
# include <sys/time.h>
# include <termios.h>
# include <signal.h>
# include <sys/uio.h>
# include "../libft/libft.h"
# include "../libft/ft_malloc.h"

typedef struct			s_params
{
	char				*file;
	char				**cmd;
	char				**env;
}						t_params;

typedef struct			s_record
{
	size_t				len;
	struct timespec		rec_time;
	char				io;
}						t_record;

pid_t					g_pid;
char					*g_opts;

void					fd_communication(int fd_pty, fd_set fd_read, int file);

void					launch_typescript(int fd, t_params params);

void					playback(int fd, off_t size);
void					record(int fd, size_t len, char io);

void					recup_params(int n, char **av, t_params *params);

char					**recup_path_shell(char **env);
char					**recup_path_bin(char **av, t_params *params);

#endif
